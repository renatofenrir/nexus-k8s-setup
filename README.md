# Nexus Repository Manager Kubernetes Setup


### This little project is intended to provide a brief explanation about how to deploy a Nexus Repository Manager to a k8s cluster.

For more details, check these pages: 

- https://devopscube.com/setup-nexus-kubernetes/
- https://blog.sonatype.com/using-nexus-3-as-your-repository-part-3-docker-images
- https://guides.sonatype.com/repo3/technical-guides/secure-docker-registries/
- https://blog.sonatype.com/kubernetes-recipe-sonatype-nexus-3-as-a-private-docker-registry



So first things, first. 


Create a **namespace** called **devops-tools**. You can change this to any other name, 
but if you want to do so, remember to edit the yaml files.


```
kubectl create namespace devops-tools
```

Afterwards, it is possible to check its creation by issuing the following command:

```
$ kubectl get namespace
NAME           STATUS   AGE
default        Active   37d
devops-tools   Active   106m
kube-public    Active   37d
kube-system    Active   37d
monitoring     Active   36d
```

Cool. Now create a yaml file called **nexus-deployment.yaml** with the content bellow:


```
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: nexus
  namespace: devops-tools
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: nexus-server
    spec:
      containers:
        - name: nexus
          image: sonatype/nexus3:latest
          resources:
            limits:
              memory: "4Gi"
              cpu: "1000m"
            requests:
              memory: "2Gi"
              cpu: "500m"
          ports:
            - containerPort: 8081
          volumeMounts:
            - name: nexus-data
              mountPath: /nexus-data
      volumes:
        - name: nexus-data
          emptyDir: {}
```

Then afterwards, issue the following command to apply the deployment file and create the deployment for Nexus:


```
kubectl apply -f nexus-deployment.yaml
```

Cool. Now check if this deployment was successfuly created by issuing the following command:


```
$ kubectl get deployments --all-namespaces
NAMESPACE      NAME                         READY   UP-TO-DATE   AVAILABLE   AGE
default        mygrafana                    1/1     1            1           19d
default        traefik-ingress-controller   1/1     1            1           33d
devops-tools   nexus                        1/1     1            1           62m
kube-system    calico-typha                 0/0     0            0           37d
kube-system    coredns                      2/2     2            2           37d
kube-system    kubernetes-dashboard         1/1     1            1           46m
kube-system    tiller-deploy                1/1     1            1           33d
monitoring     prometheus-deployment        1/1     1            1           29d

```


Great. We now have a deployment for nexus up and running, but still we have to expose its 
pods by creating a service. Create a file called **nexus-service.yaml** with the following content:


```
apiVersion: v1
kind: Service
metadata:
  name: nexus-service
  namespace: devops-tools
spec:
  ports:
  - port: 80
    targetPort: 8081
    protocol: TCP
    name: http
  - port: 5000
    targetPort: 5000
    protocol: TCP
    name: docker 
  selector:
    app: nexus-server
```

And then, execute the following command to apply this service file:


```
kubectl apply -f nexus-service.yaml
```

Great. Now check if it's running by issuing the following command:


```
$ kubectl get svc --all-namespaces |grep nexus
devops-tools   nexus-service          NodePort    10.100.91.172    <none>        8081:32000/TCP            66m
```

Excellent. Now we have a service that is going to expose the Nexus port for the docker registry, 
so it can be reached from outside.

To get further information about this service, you can also issue the following command:


```
$ kubectl describe svc nexus-service --namespace=devops-tools
Name:                     nexus-service
Namespace:                devops-tools
Labels:                   <none>
Annotations:              kubectl.kubernetes.io/last-applied-configuration:
                            {"apiVersion":"v1","kind":"Service","metadata":{"annotations":{"prometheus.io/path":"/","prometheus.io/port":"8081","prometheus.io/scrape"...
                          prometheus.io/path: /
                          prometheus.io/port: 8081
                          prometheus.io/scrape: true
Selector:                 app=nexus-server
Type:                     NodePort
IP:                       10.100.91.172
Port:                     <unset>  8081/TCP
TargetPort:               8081/TCP
NodePort:                 <unset>  32000/TCP
Endpoints:                172.16.2.13:8081
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

Also, it is necessary to deploy an ingress for this setup, so we can have a nice URL to access Nexus gui and 
we're going to have a url to use with the registry:


```
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: nexus-ingress
  namespace: devops-tools
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/proxy-body-size: 500m
    ingress.kubernetes.io/proxy-body-size: 500m
spec:
  tls:
  - hosts:
    # CHANGE ME
    - docker.rodeo.io
    - nexus.rodeo.io
    #secretName: nexus-tls
  rules:
  # CHANGE ME
  - host: nexus.rodeo.io # change to other url.. this has to be like nexus.yourdomain.com format
    http:
      paths:
      - path: /
        backend:
          serviceName: nexus-service
          servicePort: 80
  # CHANGE ME
  - host: docker.rodeo.io # change to other url.. this has to be like docker.yourdomain.com format
    http:
      paths:
      - path: /
        backend:
          serviceName: nexus-service
          servicePort: 5000
```

Now, create the ingress by issuing the following command using kubectl:


```
kubectl create -f nexus-ingress.yaml
```

Afterwards, you can check if this one is already up and running by issuing the following:


```
kubectl get pods --all-namespaces |grep ingress
```

And you'll get something similar to this output:


```
kubectl get pods --all-namespaces |grep ingress
ingress-nginx   nginx-ingress-controller-cqj2m            1/1       Running     1          20h
```


Now go check the Nexus gui by accessing the url definied in the ingress configs:

```
http://nexus.yourdomain.com
```

obs: not yet.. put the configuration missing right here.. :P
you have to put how to configure docker for a secured and non-secured registry setup 
and also put information about the procedures of creating the registry repo itself 
via nexus gui. put some prints and it should be ok.. 

Cool. Now we have a Nexus Repository Manager up and running. Cheers!